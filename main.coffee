# std packages
fs = require 'fs'

# non-std packages
Steam = require 'steam'

# local packages
users = require './users.coffee'

if not fs.existsSync 'settings.json'
	console.log 'Missing settings.json file'
	process.exit 1
	return

settings = JSON.parse fs.readFileSync('settings.json')

cache = {}
if fs.existsSync 'cache.json'
	cache = JSON.parse fs.readFileSync('cache.json')
	Steam.servers = cache.servers
cache.users ?= {}

bot = new Steam.SteamClient
bot.on 'error', (e) ->
	console.log e
	process.exit 1

bot.logOn {
	accountName: settings.accountName
	password: settings.password
}

bot.on 'loggedOn', ->
	console.log 'Logged in!'
	bot.setPersonaState Steam.EPersonaState.Online
	bot.setPersonaName settings.displayName

bot.on 'servers', (servers) ->
	cache.servers = servers
	fs.writeFile 'cache.json', JSON.stringify(cache, null, '\t')

bot.on 'user', (user) ->
	switch user.personaState
		when Steam.EPersonaState.Online
			console.log "#{user.playerName} just came online"
		when Steam.EPersonaState.Away
			console.log "#{user.playerName} is now away"
		when Steam.EPersonaState.Busy
			console.log "#{user.playerName} is now busy"
		when Steam.EPersonaState.Offline
			console.log "#{user.playerName} just logged off"

	cache.users[user.friendid] = user

bot.on 'friend', (sid, friendStatus) ->
	# TODO: Maybe display the name of who added us
	# TODO: Display when someone unfriends us
	if friendStatus is Steam.EFriendRelationship.PendingInvitee
		bot.addFriend sid
		bot.sendMessage sid, settings.welcomeText
	u = new users.User sid, bot
	if u.displayName?
		console.log "Adding #{u.displayName}"
	else
		console.log "Adding new friend (#{sid})"

bot.on 'relationships', ->
	# TODO: Maybe display the name of who added us
	for sid, status of bot.friends
		if status is Steam.EFriendRelationship.PendingInvitee
			bot.addFriend sid
			bot.sendMessage sid, settings.welcomeText
			u = new users.User sid, bot
			if u.displayName?
				console.log "Adding #{u.displayName}"
			else
				console.log "Adding new friend (#{sid})"

bot.on 'message', (source, message, type, chatter) ->
	# We only respond to chat messages
	if type isnt Steam.EChatEntryType.ChatMsg
		return

	try
		data = /^!([a-z]+)(?:\s(.+))?$/.exec message
		if not data?
			throw settings.welcomeText

		s = new users.User source, bot

		cmd = data[1]
		text = data[2]
		args = text?.split(' ')

		switch cmd
			when 'ping'
				bot.sendMessage source, 'pong'
			when 'sid'
				bot.sendMessage source, source
			when 'seed', 'seedip', 'seednotip'
				if not s.get('seeder')
					throw "You are not a seeder"

				if cmd is 'seednotip' and args?.length < 2
					throw 'Usage: !seedip excludedips message'
				else if cmd is 'seedip' and args?.length < 2
					throw 'Usage: !seedip includedips message'
				else if cmd is 'seed' and args?.length < 1
					throw 'Usage: !seed message'

				for sid, friendStatus of bot.friends
					if friendStatus isnt Steam.EFriendRelationship.Friend
						continue
					else if sid is source
						continue

					user = new users.User sid, bot
					notify_when = user.get('notify_when')

					switch user.personaState
						when Steam.EPersonaState.Online
							if 'online' not in notify_when
								continue
						when Steam.EPersonaState.Away
							if 'away' not in notify_when
								continue
						when Steam.EPersonaState.Busy
							if 'busy' not in notify_when
								continue
						else
							# Skip any unknown states
							continue

					# If they don't want in game notifications and they're in game, skip
					if 'in_game' not in notify_when and user.gameId != 0
						continue

					list_type = user.get('app_list_type')
					if user.gameId != 0 and list_type isnt 'off'
						app_in_list = user.gameId in user.get('app_list')
						if list_type is 'blacklist' and app_in_list
							continue
						else if list_type is 'whitelist' and not app_in_list
							continue

					# If we ran the ip exclusion command, do special processing
					if cmd is 'seedip' or cmd is 'seednotip'
						data = /^(.+?)\s(.+)?$/.exec text
						if not data?
							throw 'An error occured while parsing the command'

						# Process the IP list
						# TODO: Move this out of the loop, unless we want to keep it at O(n*m)
						exclusions = []
						initial_exclusions = data[1].split(',')
						text = data[2]
						for ip in initial_exclusions
							ipr = settings.ipReplacements[ip]
							if ipr?
								exclusions = exclusions.concat settings.ipReplacements[ip]
							else
								exclusions.push ip

						if cmd is 'seednotip'
							if user.gameIp in exclusions
								continue
						else if cmd is 'seedip'
							if user.gameIp not in exclusions
								continue

					bot.sendMessage source, "Sending message to #{user.displayName}"
					bot.sendMessage sid, "#{s.displayName}: #{text}"
			when 'get'
				if args?.length isnt 1
					throw 'Usage: !get prop'

				val = s.get args[0]
				bot.sendMessage source, "#{args[0]}: #{val}"
			when 'getuser'
				if args?.length isnt 2
					throw 'Usage: !getuser userid prop'

				u = new users.User args[0], bot
				val = u.get args[1], s
				bot.sendMessage source, "#{args[1]}: #{val}"
			when 'set'
				if args?.length isnt 2
					throw 'Usage: !set prop value'

				s.set args[0], args[1]
				bot.sendMessage source, "Set #{args[0]} to #{args[1]}"
			when 'setuser'
				if args?.length isnt 3
					throw 'Usage: !setuser userid prop value'

				u = new users.User args[0], bot
				u.set args[1], args[2], s
				bot.sendMessage source, "Set #{args[1]} to #{args[2]}"
			when 'unset'
				if args?.length isnt 1
					throw 'Usage: !unset prop'

				s.unset args[0]
				bot.sendMessage source, "Unset #{args[0]}"
			when 'unsetuser'
				if args?.length isnt 2
					throw 'Usage: !unset userid prop'

				u = new users.User args[0], bot
				u.unset args[1], s
				bot.sendMessage source, "Unset #{args[1]}"
			else
				throw "Unknown command"

	catch e
		if typeof e is 'string'
			bot.sendMessage source, e
		else
			bot.sendMessage source, "An internal error occured."
			console.log e
			console.log e.stack
