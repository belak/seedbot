var steam2Check = /^STEAM_[0-5]:([0-1]):([0-9]+)$/;
var steam3Check = /^\[U:1:([0-9]+)\]$/;
var communityCheck = /^7656119([0-9]{10})$/;

exports.normalize = function(sid) {
	// sid conversion based on https://github.com/xPaw/SteamID/blob/master/SteamID.js

	// Check which type of SID we're getting.
	// Note that there are 3 types we care about:
	// * Steam Community ID (what we need)
	// * Steam ID
	// * New Steam ID
	if (sid.match(communityCheck) != null) {
		return sid;
	}

	var accountId = null;
	var match = steam2Check.match(sid);
	if (match != null) {
		accountID = (match[2] << 1) | match[1];
		return '7656119' + (7960265728 + accountID);
	} else {
		var match = steam3Check.match(sid);
		if (match != null) {
			accountID = parseInt(match[1]);
			return '7656119' + (7960265728 + accountID);
		}
	}

	// TODO: Fall back to profile URL
	throw 'That does not appear to be a valid Steam ID';
}
