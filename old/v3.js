var fs = require('fs');

// Non-std packages
var ip = require('ip');
var Steam = require('steam');

// Local packages
var User = require('./user').User
var normalize = require('./steamid').normalize

// Load settings
if (!fs.existsSync('settings.json')) {
	console.log('Missing settings.json file');
	process.exit(1);
	return;
}

var settings = JSON.parse(fs.readFileSync('settings.json'));

// Load the cache
var cache = {};
if (fs.existsSync('cache.json')) {
	cache = JSON.parse(fs.readFileSync('cache.json'));
	if (cache.users == undefined) {
		cache.users = {};
	}
	Steam.servers = cache.servers;
}

var bot = new Steam.SteamClient();
bot.on('error', function(e) {

}
