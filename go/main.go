package main

/*
   TODO:
    Make this work with a config file
    Add chat functionality
	Add web interface
	figure out why we get tons of extra state changes
*/

import (
	"log"
	"reflect"

	"github.com/Philipp15b/go-steam"
	"github.com/Philipp15b/go-steam/internal/steamlang"
)

func main() {
	myLoginInfo := steam.LogOnDetails{}
	myLoginInfo.Username = "username"
	myLoginInfo.Password = "password"

	client := steam.NewClient()
	client.Connect()
	for event := range client.Events() {
		switch e := event.(type) {
		case *steam.ConnectedEvent:
			// Now that we're connected, we need to actually log in
			log.Printf("Logging in as %s\n", myLoginInfo.Username)
			client.Auth.LogOn(myLoginInfo)
		case *steam.LoggedOnEvent:
			// Our login was successful, so set the state to online
			log.Println("Logged in!")
			client.Social.SetPersonaState(steamlang.EPersonaState_Online)
			// TODO: displayName?
		case *steam.PersonaStateEvent:
			f, _ := client.Social.Friends.ById(e.FriendId)

			// Display when someone changes state
			state := "unknown"

			switch e.State {
			case steamlang.EPersonaState_Offline:
				state = "offline"
			case steamlang.EPersonaState_Online:
				state = "online"
			case steamlang.EPersonaState_Busy:
				state = "busy"
			case steamlang.EPersonaState_Away:
				state = "away"
			case steamlang.EPersonaState_Snooze:
				state = "snoozing"
			case steamlang.EPersonaState_LookingToTrade:
				state = "looking to trade"
			case steamlang.EPersonaState_LookingToPlay:
				state = "looking to play"
			}

			log.Printf("%s is now %s\n", e.Name, state)
		case *steam.FriendsListEvent:
			// Now that we have a friends list, we loop through and check for friend requests
			for sid, friend := range client.Social.Friends.GetCopy() {
				if friend.Relationship == steamlang.EFriendRelationship_RequestRecipient {
					log.Printf("Adding friend %s\n", sid)
					client.Social.AddFriend(sid)
				}
			}
		case *steam.FriendStateEvent:
			if e.Relationship == steamlang.EFriendRelationship_RequestRecipient {
				log.Printf("Adding friend %s\n", e.SteamId)
				client.Social.AddFriend(e.SteamId)
			}
		case *steam.ChatMsgEvent:
			if !e.IsMessage() {
				continue
			}

			log.Printf("MSG: %s\n", e)

		//case *steam.LoginKeyEvent:

		// Messages to ignore
		case *steam.AccountInfoEvent:
		case *steam.ClientCMListEvent:
		case *steam.ClanStateEvent:
		case *steam.WebSessionIdEvent:

		// Errors
		case *steam.FatalErrorEvent:
			log.Print(e)
		case error:
			log.Print(e)

		// Unknown
		default:
			log.Printf("%s: %s\n", reflect.TypeOf(e), e)
		}
	}
}
